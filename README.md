# Knight

- Bar: [polybar](https://polybar.github.io/)
- Fetch: [macchina](https://github.com/macchina-io/macchina.io)
- Browser: [firefox](https://firefox.com)
- File Browser: [ranger](https://github.com/ranger/ranger)
- Notifications: [dunst](https://github.com/dunst-project/dunst)

## Preview
![Preview](https://i.redd.it/f7f871jxi7o81.png)
